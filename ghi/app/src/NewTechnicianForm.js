import React, { useState } from 'react';

const NewTechnicianForm = () => {

    const [techName, setTechName] = useState('');
  const [employeeNumber, setEmployeeNumber] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    const newTech = {
      'name': techName,
      'employee_number': employeeNumber,
    }

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(newTech),
      headers: {'Content-Type': 'application/json'},
    };

    
    fetch(technicianUrl, fetchConfig)
      .then(response => response.json())
      .then( () => {
        setTechName('');
        setEmployeeNumber('');
      })
      .catch(e => console.log("Error: ", e));
  }


  const handleTechNameChange = (event) => {
    const value = event.target.value;
    setTechName(value);
  }
  const handleEmployeeNumberChange = (event) => {
    const value = event.target.value;
    setEmployeeNumber(value);
  }


    return (
      <div className="row">
          <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                  <h1>Add a New Technician</h1>
                  <form onSubmit={handleSubmit} id="create-bin-form">
                      <div className="form-floating mb-3">
                          <input value={techName} onChange={handleTechNameChange} required type="text" name="techName" id="techName" className="form-control" />
                          <label>Technician Name</label>
                      </div>
                      <div className="form-floating mb-3">
                          <input value={employeeNumber} onChange={handleEmployeeNumberChange} required type="number" name="employeeNumber" id="employeeNumber" className="form-control" />
                          <label>Enter Employee Number</label>
                      </div>
                      <button className="btn btn-primary">Create</button>
                  </form>
              </div>
          </div>
      </div>
  );
}

export default NewTechnicianForm;
