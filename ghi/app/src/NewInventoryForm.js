import React, { useState, useEffect} from "react"

function NewInventoryForm() {

    const [models, setModels] = useState([])
    const [color, setColors] = useState('')
    const [year, setYear] = useState('')
    const [VIN, setVIN] = useState('')
    const [model, setModel] = useState('')

    const handleColorChange = event => {
        const value = event.target.value
        setColors(value)
    }

    const handleYearChange = event => {
        const value = event.target.value
        setYear(value)
    }

    const handleVINChange = event => {
        const value = event.target.value
        setVIN(value)
    }

    const handleModelChange = event => {
        const value = event.target.value
        setModel(value)
    }

    const handleSubmit = async event => {
        event.preventDefault()

        const data = {
            color,
            year,
        }
        data.vin = VIN
        data.model_id = model

        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method:'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body:JSON.stringify(data),
        }

        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            const newAutomobile = await response.json()
            setColors('')
            setYear('')
            setVIN('')
            setModel('')
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }
    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an automobile to inventory</h1>
            <form id="create-inventory-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Color" value={color} required type="text" name="color" id="color" className="form-control" onChange={handleColorChange}/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Year" value={year} required type="text" name="year" id="year" className="form-control" onChange={handleYearChange}/>
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="VIN" value={VIN} required type="text" name="VIN" id="VIN" className="form-control" onChange={handleVINChange}/>
                <label htmlFor="VIN">VIN</label>
              </div>
              <div className="mb-3">
                <select required value={model} name="model" id="model" className="form-select" onChange={handleModelChange}>
                  <option value="">Choose a model</option>
                  {models.map(model => {
                    return (
                    <option key={model.id} value={model.id}>
                        {model.name}
                    </option>
                    )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default NewInventoryForm;
