import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-info">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
            <NavLink className="nav-link" to="manufacturers/">MANUFACTURERS</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" to="models/">MODELS</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/inventory">INVENTORY</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" to="appointments/history">SERVICE HISTORY</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" to="appointments/">APPOINTMENTS</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" to="technicians/" >TECHNICIANS</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers/new">ADD CUSTOMER</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales_record/new">CREATE SALES RECORD</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales">SALES HISTORIES</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales_record/sales_person">SALES BY EMPLOYEE</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
{/* <nav className="navbar navbar-expand-lg navbar-dark bg-success">
<div className="container-fluid">
  <NavLink className="navbar-brand" to="/">CarCar</NavLink>
  <NavLink className="navbar-brand" to="manufacturers/">List Manufacturers</NavLink>
  <NavLink className="navbar-brand" to="manufacturers/new/">Create a Manufacturer</NavLink>
  <NavLink className="navbar-brand" to="models/">List Models</NavLink>
  <NavLink className="navbar-brand" to="models/new/">Create a Model</NavLink>
  <NavLink className="navbar-brand" to="automobiles/">List Automobiles</NavLink>
  <NavLink className="navbar-brand" to="automobiles/new/">Create an Automobile</NavLink>
  <NavLink className="navbar-brand" to="appointments/history">Service History</NavLink>
  <NavLink className="navbar-brand" to="appointments/">Service Appointments</NavLink>
  <NavLink className="navbar-brand" to="appointments/new">Create Service Appointment</NavLink>
  <NavLink className="navbar-brand" to="technicians/" >Technicians</NavLink>
  <NavLink className="navbar-brand" to="technicians/new">Add a Technician</NavLink>
  <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
      <li className="nav-item">
        <NavLink className="nav-link" to="/models/new">Create Vehicle Model</NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/inventory/new">Add to Inventory</NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/inventory">Inventory</NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/sales/new">Add Sales Person</NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/customers/new">Add Customer</NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/sales_record/new">Create a Sale Record</NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/sales">All Sales</NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/sales_record/sales_person">Sales by Sales Person</NavLink>
      </li>
    </ul>
  </div>
</div>
</nav> */}
