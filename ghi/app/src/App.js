import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect , useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentsList from './AppointmentsList';
import AppointmentForm from './AppointmentForm';
import ManufacturerList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import ServiceHistory from './ServiceHistory';
import NewModelForm from './NewModelForm';
import NewInventoryForm from './NewInventoryForm';
import InventoryList from './InventoryList';
import NewSalesPersonForm from './NewSalesPersonForm';
import NewPotentialCustomerForm from './NewPotentialCustomerForm';
import AllSalesList from './AllSalesList';
import SalesPersonHistoryList from './SalesPersonHistoryList';
import NewSaleRecordForm from './NewSaleRecordForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">


        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="appointments">
            <Route path="" element={<AppointmentsList/>}/>
            <Route path="new" element={<AppointmentForm/>}/>
            <Route path="history"  element={<ServiceHistory/>}/>
          </Route>
          <Route path="technicians">
            <Route path="" element={<TechnicianList/>}/>
            <Route path="new" element={<TechnicianForm/>} />
          </Route>
          <Route path="inventory">
              <Route path="all" element={<ModelList/>} />
          </Route>
          <Route path="manufacturers">
              <Route path="" element={<ManufacturerList />} />
              <Route path="new" element={<ManufacturerForm/>} />
          </Route>
          <Route path="models">
            <Route path="" element={<ModelList/>} />
            <Route path='new' element={<NewModelForm />}/>
          </Route>
          <Route path="inventory">
            <Route path='new' element={<NewInventoryForm />}/>
            <Route path='' element={<InventoryList />}/>
          </Route>
          <Route path="sales">
            <Route path='new' element={<NewSalesPersonForm />}/>
            <Route path='' element={<AllSalesList />}/>
          </Route>
          <Route path="sales_record">
            <Route path='new' element={<NewSaleRecordForm />}/>
            <Route path='sales_person' element={<SalesPersonHistoryList />}/>
          </Route>
          <Route path="customers">
            <Route path='new' element={<NewPotentialCustomerForm />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
