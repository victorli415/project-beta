
import { useEffect , useState } from 'react';
import { NavLink } from 'react-router-dom';

function ModelList() {

  const [models, setModels] = useState([])


  // const deleteModel = async (id) => {
  //   fetch(`http://localhost:8100/api/models/${id}/`, {
  //     method:"delete",
  //   })
  //   .then(() => {
  //     return getModels()
  //   })
  // }

  // if (models === undefined) {
  //   return null
  // }

  useEffect(() => {
    const fetchModels = async () => {
      const url = "http://localhost:8100/api/models/"
      const response = await fetch(url)

      if (response.ok) {
        const data = await response.json()
        setModels(data.models)
      }
    }
    if (models){
      fetchModels()
    }
  }, [models])



  return (
  <>
  <NavLink className="navbar-brand" to="/models/new">Add Vehicle Model</NavLink>
  <h1 className="mb-3 mt-3 text-center">MODELS</h1>
    <table className="table table-striped">
      <thead>
        <tr>

          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>

        </tr>
      </thead>
      <tbody>
        {models.map((model) => {
          return (
            <tr key={model.id}>
              <td>{ model.name }</td>
              <td>{ model.manufacturer.name }</td>
              <td><img src={model.picture_url} height="100" width="100"></img></td>

              {/* <td>
                  <button type="button" value={model.id} onClick={() => deleteModel(model.id)}>Delete</button>
              </td> */}
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}

export default ModelList;
