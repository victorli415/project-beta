import React, { useState, useEffect} from "react";

function NewModelForm() {
    const [manufacturers, setManufacturers] = useState([])
    const [name, setName] = useState('')
    const [picture_url, setPictureUrl] = useState('')
    const [manufacturer, setManufacturer] = useState('')

    const handleNameChange = event => {
        const value = event.target.value
        setName(value)
    }

    const handleManufacturerChange = event => {
        const value = event.target.value
        setManufacturer(value)
    }

    const handlePictureUrlChange = event => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            name,
            picture_url,
        }
        data.manufacturer_id = manufacturer

        const url = 'http://localhost:8100/api/models/'
        const fetchData = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }
        const response = await fetch(url, fetchData)
        if (response.ok) {
            const newModel = await response.json()
            setName('')
            setPictureUrl('')
            setManufacturer('')
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }
    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a vehicle model</h1>
            <form id="create-vehicle-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Name" value={name} required type="text" name="name" id="name" className="form-control" onChange={handleNameChange}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Picture Url" value={picture_url} required type="url" name="picture_url" id="picture_url" className="form-control" onChange={handlePictureUrlChange}/>
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select required value={manufacturer} name="manufacturer" id="manufacturer" className="form-select" onChange={handleManufacturerChange}>
                  <option value="">Choose a manufacturer</option>
                  {manufacturers.map(manufacturer => {
                    return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                    </option>
                    )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default NewModelForm;
