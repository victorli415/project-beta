import React, { useState,} from "react"

function NewPotentialCustomerForm() {

    const [name, setName] = useState('')
    const [address, setAddress] = useState('')
    const [phone_number, setNumber] = useState('')


    const handleNameChange = event => {
        const value = event.target.value
        setName(value)
    }

    const handleAddressChange = event => {
        const value = event.target.value
        setAddress(value)
    }

    const handleNumberChange = event => {
        const value = event.target.value
        setNumber(value)
    }


    const handleSubmit = async event => {
        event.preventDefault()

        const data = {
            name,
            address,
            phone_number,
        }


        const url = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method:'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body:JSON.stringify(data),
        }

        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            setName('')
            setNumber('')
            setAddress('')
        }

    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Potential Customer</h1>
            <form id="create-potential-customer-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Color" value={name} required type="text" name="color" id="color" className="form-control" onChange={handleNameChange}/>
                <label htmlFor="color">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Address" value={address} required type="text" name="address" id="address" className="form-control" onChange={handleAddressChange}/>
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Phone" value={phone_number} required type="number" name="phone_number" id="phone_number" className="form-control" onChange={handleNumberChange}/>
                <label htmlFor="Phone">Phone number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default NewPotentialCustomerForm;
