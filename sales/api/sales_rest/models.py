from django.db import models
from django.urls import reverse

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin

class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_salesperson", kwargs={"pk": self.id})

class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.PositiveBigIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})


class SaleRecord(models.Model):
    sale_price = models.PositiveBigIntegerField()
    vin = models.ForeignKey(
        AutomobileVO,
        verbose_name='vin',
        on_delete=models.CASCADE,
        )
    sales_person = models.ForeignKey(
        SalesPerson,
        on_delete=models.CASCADE,
        verbose_name='sales_person',
        )
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        verbose_name='customer',
        )

    def get_api_url(self):
        return reverse("api_sales", kwargs={"id": self.pk})

    class Meta:
        ordering = ("sales_person",'customer', 'vin', 'sale_price',)
