import React, { useState, useEffect } from "react"
import { NavLink } from "react-router-dom"


function SalesPersonHistoryList() {
    const [salesPeople, setSalesPeople] = useState([])
    const [salesRecords, setSalesRecords] = useState([])

    const [salesId, setSalesId] = useState('')

    const handlePersonChange = async (event) => {
        const value = event.target.value
        setSalesId(value)
    }


    useEffect(() => {
        const fetchHistoryData = async () => {
            const sales_history_url = `http://localhost:8090/api/sales/${salesId}/`
            const sales_history_response = await fetch(sales_history_url)

            if (sales_history_response.ok) {
                const sales_history_data = await sales_history_response.json()
                setSalesRecords(sales_history_data)
            }
        }
        if (salesId) {
            fetchHistoryData()
        }
      }, [salesId]);

    useEffect(() => {
    const fetchPeopleData = async () => {
        const sales_people_url = 'http://localhost:8090/api/sales_people/'
        const sales_people_response = await fetch(sales_people_url)

        if (sales_people_response.ok) {
            const sales_people_data = await sales_people_response.json()
            setSalesPeople(sales_people_data.sales_people)
        }
    }

    if (salesPeople) {
        fetchPeopleData();
    }
      }, [salesPeople]);


    return (
        <>
        <h1 className="mb-3 mt-3 text-center">EMPLOYEE SALES RECORD</h1>
        <div className="mb-3">
            <select value={salesId} required name="sales_people" id="sales_people" className="form-select" onChange={handlePersonChange}>
                <option value="">Choose a Sales Person</option>
                {salesPeople.map(person => {
                    return (
                        <option key={person.id} value={person.id} >
                            {person.name}
                        </option>
                    )
                })}
            </select>
        </div>
        <table className="table table-striped">
              <thead>
                <tr>
                <th>Sales Person</th>
                <th>Employee Number</th>
                <th>Purchaser Name</th>
                <th>Automobile VIN</th>
                <th>Price of Sale</th>
                </tr>
            </thead>
            <tbody>
                {salesRecords.map(sale =>{
                return (
                    <tr key={ sale.vin.vin }>
                    <td>{ sale.sales_person.name }</td>
                    <td>{ sale.sales_person.employee_number }</td>
                    <td>{ sale.customer.name }</td>
                    <td>{ sale.vin.vin }</td>
                    <td>{ sale.sale_price }</td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </>
  );
}


export default SalesPersonHistoryList;
