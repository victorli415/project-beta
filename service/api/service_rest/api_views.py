from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import  Technician, Appointment,AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
        "color",
        "year",
        "model_name",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "technician_name",
        "employee_number",
        "id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "date_time",
        "reason",
        "technician",
        "is_completed",
        "vip",
        "id",
        "cancelled",
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }
@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments":appointments},
            encoder=AppointmentEncoder,
        )
    elif request.method == "POST":
        new_content = {}
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician_name"])
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message":"This technician does not exist"}
            )
        new_content["vin"] = content["vin"]
        new_content["customer_name"] = content["customer_name"]
        new_content["date_time"] = content["date_time"]
        new_content["reason"] = content["reason"]
        new_content["technician"] = technician
        new_content["is_completed"] = False
        new_content["cancelled"] = False
        new_content["vip"] = False

        if AutomobileVO.objects.filter(vin=new_content["vin"]).exists():
            new_content["vip"] = True



        appointment = Appointment.objects.create(**new_content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_appointment(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder = AppointmentEncoder,
                safe = False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status = 404,
            )

    elif request.method == "DELETE":
        count, _ = Appointment.objects.get(id=id).delete()
        return JsonResponse({"delete": count > 0})

    elif request.method =="PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder = AppointmentEncoder,
            safe = False
        )


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create the technician"},
                code = 400,
            )


@require_http_methods(["DELETE"])
def api_technician(request, id):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0})


@require_http_methods(["GET"])
def api_appointments_by_vin(request, vin):
    try:
        if request.method == "GET":
            appointments_by_vin = Appointment.objects.filter(vin=vin)
            return JsonResponse(
                appointments_by_vin,
                encoder = AppointmentEncoder,
                safe=False
            )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Does not exist"},
            code = 404
        )

def api_automobile_VO_list(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos":autos},
            encoder = AutomobileVOEncoder
        )
