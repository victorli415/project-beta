import { useEffect , useState } from 'react';
import { NavLink } from 'react-router-dom';
function ManufacturerList() {

  const [manufacturers, setManufacturers] = useState([])

  // const deleteManufacturer = async (id) => {
  //   fetch(`http://localhost:8100/api/manufacturers/${id}/`, {
  //     method:"delete",
  //   })
  //   .then(() => {
  //     return getManufacturers()
  //   })
  // }


  // if (manufacturers === undefined) {
  //   return null
  // }

    useEffect(() => {
  const fetchManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }
  if (manufacturers){
    fetchManufacturers()
  }
}, [manufacturers])


  return (
  <>
    <NavLink className="navbar-brand" to="/manufacturers/new/">Add a Manufacturer</NavLink>
    <h1 className="mb-3 mt-3 text-center">MANUFACTURERS</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {manufacturers.map((manufacturer) => {
          return (
            <tr key={manufacturer.id}>
              <td>{ manufacturer.name }</td>
              {/* <td>
                  <button type="button" value={manufacturer.id} onClick={() => deleteManufacturer(manufacturer.id)}>Delete</button>
              </td> */}
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}
export default ManufacturerList;
