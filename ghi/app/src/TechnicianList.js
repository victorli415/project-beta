
import { NavLink} from "react-router-dom"
import React, { useEffect, useState } from 'react';

function TechnicianList() {

    const [technicians, setTechnicians] = useState([])

//   const handleDeleteClick = async (event) => {
//     const id = event.target.value
//     const response = await fetch (`http://localhost:8080/api/technician/${id}/`,{
//       method: "delete",
//     })
//     if (response.ok){
//         props.getTechnician()
//     }
//   }

//   if (props.technicians === undefined) {
//     return null
//   }
  useEffect(() => {
    const getTechnicians = async () => {
      const url = 'http://localhost:8080/api/technicians/'
      const response = await fetch(url)

      if (response.ok) {
        const data = await response.json()
        const technicians = data.technicians
        setTechnicians(technicians)
      }
    }
  if (technicians){
    getTechnicians()
  }
  }, [technicians])

  return (
    <div>
      <NavLink className="navbar-brand" to="/technicians/new">Add a New Technician</NavLink>
      <h1 className="mb-3 mt-3 text-center">TECHNICIANS</h1>
      <table className="table table-striped table-hover align-middle mt-5">
        <thead>
          <tr>
            <th>Technician Name</th>
            <th>Employee Number</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map(technician => {
            return (
              <tr key={technician.id}>
                <td>{ technician.technician_name }</td>
                <td>{ technician.employee_number }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}

export default TechnicianList;
