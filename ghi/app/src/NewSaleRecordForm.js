import React, { useState, useEffect} from "react"

function NewSaleRecordForm() {

    const [vins, setVins] = useState([])
    const [vin, setVin] = useState('')

    const [people, setPeople] = useState([])
    const [person, setPerson] = useState('')

    const [customers, setCustomers] = useState([])
    const [customer, setCustomer] = useState('')

    const [price, setPrice] = useState('')

    const handleVinChange = event => {
        const value = event.target.value
        setVin(value)
    }

    const handlePersonChange = event => {
        const value = event.target.value
        setPerson(value)
    }

    const handleCustomerChange = event => {
        const value = event.target.value
        setCustomer(value)
    }

    const handlePriceChange = event => {
        const value = event.target.value
        setPrice(value)
    }


    const handleSubmit = async event => {
        event.preventDefault()

        const data = {vin,}
        data.sales_person_id = person
        data.customer_id = customer
        data.sale_price = price


        const url = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method:'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body:JSON.stringify(data),
        }

        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            const auto_url =`http://localhost:8100/api/automobiles/${vin}`
            fetch(auto_url,{ method: 'DELETE' })
            .then(() =>
            {
                setVin('')
                setPerson('')
                setCustomer('')
                setPrice('')
            })
        }

    }
    useEffect(() => {
    const fetchData = async () => {
        const auto_url = 'http://localhost:8100/api/automobiles/'
        const person_url = 'http://localhost:8090/api/sales_people/'
        const customer_url = 'http://localhost:8090/api/customers/'

        const auto_response = await fetch(auto_url)
        const person_response = await fetch(person_url)
        const customer_response = await fetch(customer_url)

        if (auto_response.ok) {
            const auto_response_data = await auto_response.json()

            setVins(auto_response_data.autos)
        }
        if (person_response.ok) {
            const person_response_data = await person_response.json()

            setPeople(person_response_data.sales_people)
        }
        if (customer_response.ok) {
            const customer_response_data = await customer_response.json()

            setCustomers(customer_response_data.customers)
        }

    }
    {
        fetchData();
    }
}, [vin, person, customer]);

    // useEffect(() => {
    //     fetchData();
    //     }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Sale Record</h1>
            <form id="create-sales-record-form" onSubmit={handleSubmit}>
            <div className="mb-3">
            <select value={vin} required name="vin" id="vin" className="form-select" onChange={handleVinChange}>
                <option value="">Select an Automobile</option>
                {vins.map(vin => {
                    return (
                        <option key={vin.vin} value={vin.vin}>
                            {vin.model.name}
                        </option>
                    )
                })}
            </select>
        </div>
        <div className="mb-3">
            <select value={person} required name="people" id="people" className="form-select" onChange={handlePersonChange}>
                <option value="">Select a Sales Person</option>
                {people.map(person => {
                    return (
                        <option key={person.id} value={person.id}>
                            {person.name}
                        </option>
                    )
                })}
            </select>
        </div>
        <div className="mb-3">
            <select value={customer} required name="sales_customer" id="sales_customer" className="form-select" onChange={handleCustomerChange}>
                <option value="">Select a Customer</option>
                {customers.map(customer => {
                    return (
                        <option key={customer.id} value={customer.id}>
                            {customer.name}
                        </option>
                    )
                })}
            </select>
        </div>
            <div className="form-floating mb-3">
                <input placeholder="Sales_Price" value={price} required type="number" name="sale_price" id="sale_price" className="form-control" onChange={handlePriceChange}/>
                <label htmlFor="Sales_Price">Sales Price</label>
            </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default NewSaleRecordForm;
