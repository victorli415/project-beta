from django.shortcuts import render
from .models import SalesPerson, Customer, AutomobileVO, SaleRecord
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json


# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'import_href',
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        'address',
        'phone_number',
        'id',]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name",'employee_number', 'id']

class SalesListEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        'sales_person',
        'customer',
        'sale_price',
        'vin',
        'id',
    ]
    encoders = {
        'vin': AutomobileVOEncoder(),
        'sales_person': SalesPersonEncoder(),
        'customer': CustomerEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_sales_person(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_person},
            encoder=SalesPersonEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create sales person"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customers": customer},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = SaleRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            vin= content["vin"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["vin"] = automobile
            sale = SaleRecord.objects.create(**content)
            return JsonResponse(
                sale,
                encoder = SalesListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create sale record"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "POST"])
def api_list_sales_history(request, sales_person_id):
    if request.method == 'GET':
        try:
            sales = SaleRecord.objects.filter(sales_person_id=sales_person_id)
            return JsonResponse(
                sales,
                encoder=SalesListEncoder,
                safe =False
            )
        except SaleRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
