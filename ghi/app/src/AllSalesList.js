import React, { useState, useEffect } from "react"
import { NavLink } from "react-router-dom";


function AllSalesList() {
    const [sales, setSales] = useState([])

    useEffect(() => {
    const fetchData = async () => {
        const url = 'http://localhost:8090/api/sales/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }
    }
       if  (sales) {
        fetchData();
       }
      }, [sales]);

    return (
        <>
        <NavLink className="navbar-brand" to="/sales/new">Add a Sales Person</NavLink>
        <h1 className="mb-3 mt-3 text-center">SALES HISTORY</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                <th>Sales Person</th>
                <th>Employee Number</th>
                <th>Purchaser</th>
                <th>VIN</th>
                <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                return (
                    <tr key={ sale.id }>
                    <td>{ sale.sales_person.name }</td>
                    <td>{ sale.sales_person.employee_number }</td>
                    <td>{ sale.customer.name }</td>
                    <td>{ sale.vin.vin }</td>
                    <td>{ sale.sale_price }</td>
                    </tr>
                );
                })}
            </tbody>
        </table>
        </>
  );
}


export default AllSalesList;
