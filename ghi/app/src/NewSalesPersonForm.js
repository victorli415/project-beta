import React, { useState, useEffect} from "react"

function NewSalesPersonForm() {

    const [name, setName] = useState('')
    const [employee_number, setNumber] = useState('')


    const handleNameChange = event => {
        const value = event.target.value
        setName(value)
    }

    const handleNumberChange = event => {
        const value = event.target.value
        setNumber(value)
    }


    const handleSubmit = async event => {
        event.preventDefault()

        const data = {
            name,
            employee_number,
        }


        const url = 'http://localhost:8090/api/sales_people/'
        const fetchConfig = {
            method:'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body:JSON.stringify(data),
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setName('')
            setNumber('')
        }

    }

    return (
      <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Sales Person</h1>
            <form id="create-sales-person-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Color" value={name} required type="text" name="color" id="color" className="form-control" onChange={handleNameChange}/>
                <label htmlFor="color">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Employee_number" value={employee_number} required type="number" name="employee_number" id="employee_number" className="form-control" onChange={handleNumberChange}/>
                <label htmlFor="Employee_number">Employee number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );
}

export default NewSalesPersonForm;
