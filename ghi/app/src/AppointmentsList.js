import { useEffect , useState } from 'react';
import { NavLink } from 'react-router-dom';

export default function AppointmentsList(){

    const [appointments, setAppointments] = useState([])

    const cancelAppointment = async (event) => {
        const id = event.target.value
        const response = await fetch  (`http://localhost:8080/api/appointments/${id}/`,{
          method: "delete",
        })

        // if (response.ok) {
        //     getAppointments()
        // }
    }

    const finishAppointment = async (event) => {
      const id = event.target.value
      const status = {"is_completed" :true}
      const newStatus = JSON.stringify(status)

      const response = await fetch  (`http://localhost:8080/api/appointments/${id}/`,{
        method: "put",
        body: newStatus,
        headers: {
          'Content-Type': 'application/json',
      }

      })


//       if (response.ok) {
//           getAppointments()
//       }
  }

//     if (appointments === undefined) {
//         return null;
//     }

    useEffect(() => {
        const getAppointments = async () => {
          const url = 'http://localhost:8080/api/appointments/'
          const response = await fetch(url)

          if (response.ok) {
            const data = await response.json()
            const appointments = data.appointments
            setAppointments(appointments)
          }
        }
        if (appointments){
          getAppointments()
        }
    }, [appointments])

    return (
    <>
    <NavLink className="navbar-brand" to="/appointments/new">Create Service Appointment</NavLink>
      <div className='p-5 text-left'>
        <h1 className='mb-3 text-center'>SERVICE APPOINTMENTS</h1>
        </div>
        <table className="table table-striped table-hover">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Customer</th>
                <th>VIP</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>


            {appointments.map(appointment => {
                if  (appointment.is_completed === false)
                return (
                <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <td>{ appointment.customer_name }</td>
                    <td>{ appointment.vip }{ '' + appointment.vip }</td>
                    <td>{ new Date(appointment.date_time).toLocaleDateString("en-US") }</td>
                    <td>{ new Date(appointment.date_time).toLocaleTimeString([], {
                        hour: "2-digit",
                        minute: "2-digit",
                    }) }</td>
                    <td>{ appointment.technician.technician_name}</td>
                    <td>{ appointment.reason }</td>
                    <td>
                        <button
                            value={ appointment.id } onClick={cancelAppointment}
                            type="button" className="btn btn-danger">
                            Cancel
                        </button>
                    </td>
                    <td>
                        <button
                            value={ appointment.id } onClick={finishAppointment}
                            type="button" className="btn btn-success">
                            Finished
                        </button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
    </>
    );
  }
