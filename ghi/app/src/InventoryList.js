import { NavLink } from "react-router-dom";
import React, { useState, useEffect } from "react";

function InventoryList() {

    const [autos, setAutos] = useState([])

    useEffect(() => {
        const fetchAutoData = async () => {
            const url = `http://localhost:8100/api/automobiles/`
            const response = await fetch(url)

            if (response.ok) {
                const data = await response.json()
                setAutos(data.autos)
            }
        }
        if (autos) {
            fetchAutoData()
        }
      }, [autos]);

    return (
        <>
        <NavLink className="navbar-brand" to='/inventory/new'>Add To Inventory</NavLink>
        <h1 className="mb-3 mt-3 text-center">CAR INVENTORY</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {autos.map(auto => {
                return (
                    <tr key={ auto.vin }>
                    <td>{ auto.vin }</td>
                    <td>{ auto.color }</td>
                    <td>{ auto.year }</td>
                    <td>{ auto.model.name }</td>
                    <td>{ auto.model.manufacturer.name }</td>
                    </tr>
                );
                })}
            </tbody>
        </table>
        </>
  );
}


export default InventoryList;
