from django.urls import path
from .views import api_list_sales_person, api_list_customer,api_list_sales, api_list_sales_history

urlpatterns = [
    path('sales_people/', api_list_sales_person, name='api_salesperson'),
    path('customers/', api_list_customer, name='api_customers'),
    path('sales/', api_list_sales, name='api_list_sales'),
    path('sales/<int:sales_person_id>/', api_list_sales_history, name='api_list_sales_history'),


]
