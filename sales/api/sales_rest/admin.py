from django.contrib import admin
from .models import SalesPerson, Customer, AutomobileVO, SaleRecord

# Register your models here.
@admin.register(SalesPerson)
class HatAdmin(admin.ModelAdmin):
    pass
@admin.register(Customer)
class HatAdmin(admin.ModelAdmin):
    pass
@admin.register(AutomobileVO)
class HatAdmin(admin.ModelAdmin):
    pass
@admin.register(SaleRecord)
class HatAdmin(admin.ModelAdmin):
    pass
