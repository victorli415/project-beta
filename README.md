# CarCar

Team:

* Alissa - Service
* Victor - Sales


## How to Run This Application
1. Clone project into directory and swap into the directory that was cloned.
2. Run the following terminal commands in order:
    - docker volume create beta-data
    - docker-compose build
    - docker-compose up
3. Display CarCar on the browser at http://localhost:3000/


## CRUD stuff and URL stuff
Appointment Service: Localhost, Port:8080

Sales Service: Localhost, Port:8090

Inventory Service: Localhost, Port:8100


APPOINTMENT SERVICE:

- Technicians
    GET request: /api/technicians/ ............(LIST TECHNICIANS)
        Returns: {"technician_name": [{"name": "Rattatoli","employee_number": 123456789,"id": 1}]}

    POST request: /api/technicians/ ............(MAKE A TECHNICIAN)
        Body: {"name": "Rattatoli","employee_number": 123456789}


- Appointments
    GET request: /api/appointments/ ............(LIST OF ALL APPOINTMENTS)
        Returns: {"appointments": [{"vin": "Honda","customer_name": "Kalani","date_time": "2023-03-16T14:40:00+00:00","reason": "H","technician": { "technician_name": "Rattatoli", "employee_number": "123456789","id": 1}, "is_completed": true, "vip": false, "id": 1, "cancelled": false}]}

    POST request: /api/appointments/ ............(CREATE A APPOINTMENT)
        Body: {"vin": "1C3CC5FB2AN120174","customer_name":"Remy","date_time": "2022-01-18T18:20","technician_name":1,"reason":"Wipedown"}

    GET request: /api/appointments/ID/ ............(LIST OF SERVICE HISTORY)
        Returns: {"vin": "Honda","customer_name": "Kalani","date_time": "2023-03-16T14:40:00+00:00","reason": "H",
        "technician": {"technician_name": "Rattatoli","employee_number": "123456789", "id": 1},"is_completed": true, "vip": false, "id": 1,"cancelled": false}



SALES SERVICE: (ORDERED ACCORDING TO BACKEND RUBRIC)

- Sales Person Resource
    GET request: /api/sales_people/ ............(LIST OF SALES PEOPLE)
        Returns: {"sales_people": [{"name": "Clifford The Big Red Dog","employee_number": 123456789,"id": 1}]}

    POST request: /api/sales_people/ ............(MAKE A SALES PERSON)
        Body: {"name": "Clifford The Big Red Dog","employee_number": 123456789}


- Customer Resource
    GET request: /api/customers/ ............(LIST OF CUSTOMERS)
        Returns: {"customers": [{"name": "Dora The Explorer","address": "213 Cartoon Town","phone_number": 4151234122,"id": 1}]}

    POST request: /api/customers/ ............(CREATE A POTENTIAL CUSTOMER)
        Body: {"name": "Dora The Explorer","address": "211 Beep Boop","phone_number": 1234567890}


- Sales Resource
    GET request: /api/sales/ ............(LIST OF ALL SALES RECORDS)
        Returns: {"sales": [{"sales_person": {"name": "Bob The Builder","employee_number": 231213234,"id": 1},"customer": {"name": "Dora The Explorer","address": "213 Cartoon Town","phone_number": 4151234122,"id": 1},"sale_price": 694201,"vin": {"vin": "123DAFAS23123","import_href": "/api/automobiles/123DAFAS23123/"},"id": 1}]}

    POST request: /api/sales/ ............(CREATE A SALES RECORD)
        Body: {"sale_price": 696969,"sales_person_id": 1,"customer_id": 1,"vin": "JH4DB1570PS000855"}

    GET request: /api/sales/:SALES_PERSON_ID/ ............(LIST OF SALES RECORDS FOR EMPLOYEE)
        Returns: {"sales_person": {"name": "Clifford The Big Red Dog","employee_number": 123456789,"id": 1},"customer": {"name": "Dora The Explorer","address": "213 Cartoon Town","phone_number": 4151234122,"id": 1},"sale_price": 696969,"vin": {"vin": "JH4DB1570PS000858","import_href": "/api/automobiles/JH4DB1570PS000858/"},"id": 1}


## Design

https://excalidraw.com/#room=76359577dcd7c39210b7,dImlUAZl_BFm24tyyLAaqg

## Service microservice (Alissa)

Models: Technician, Appointments and AutomobileVO
(The Automobile just as Victor states below is polled from the Inventory microservice.)

Service consists of three components.
- Technician: The technician model in the backend and the NewTechnicianForm in the frontend give the ability for users to create add new technicians.
- Appointments: After creating a technician utilizing the technician model and newtechnician form, users are given the ability to create a new service appointment by inputting information such as assigning a technician to the appointment and adding extra information such as the Vin, date or time to fill out the form.
- ServiceHistory: After creating appointments there also exists a finished or canceled button, giving users the ability to log if an appointment was canceled or finished this is kept under status and is recorded in Service history where it is filtered by the chosen vehicles VIN.


## Sales microservice (Victor)

Models: The only data pulled from the Inventory microservice was the VIN number as each VIN is unique to each car and sale (integrated into AutomobileVO).

SalesRecord model consists of four components.
- Sales Price: The price in which the sale is logged as.
- VIN: The unique vin number associated with each vehicle.(Related to AutomobileVO model). One to many relationship as "ONE sales record has MANY vins to select".
- Sales Person: A representative responsible for each sale and has a name and employee number. (Related to SalesPerson model). One to many relationship as "ONE sales record has MANY sales representatives to select".
- Customer: A potential customer who wants to purchases a vehicle has a name, an address, and a phone number. (Related to Customer model). One to many relationship as "ONE sales record has MANY customers to select".


Customers and sales people are created using forms (in the frontend) which is then added to the backend. The customer and sales person data can then be used again in the frontend "make sales record form" to make a sale. All listings will update automatically without refresh on data change.

***PLEASE READ:
The customer model has no unique fields because multiple customers can have the same name(I can be Bob and you can be Bob), two different customers can live at the same address(relatives or married) and two different customers can have the same phone number(house phone).

When a sales form is SUBMITTED and only AFTER being submitted, the automobile corresponding to the automobile submitted during sale will be deleted from the AUTOMOBILE database, but will not impact the SALES HISTORY LIST and SALES REPRESENTATIVE LIST. The automobile will no longer be able to be selected again on the sales form unless the automobile is registered again in the database by adding the automobile back.
